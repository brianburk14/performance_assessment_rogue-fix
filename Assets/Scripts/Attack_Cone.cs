﻿using UnityEngine;
using System.Collections;

public class Attack_Cone : MonoBehaviour {

	public TurrentAI turrentAI;

	public bool isLeft = false;

	void Awake(){
		turrentAI = gameObject.GetComponentInParent<TurrentAI> ();
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.CompareTag ("Player")) {
			if(isLeft)
			{
				turrentAI.Attack(false);
			} else{
				turrentAI.Attack(true);
			}
		}
	}

}
